package [
  'php56w-cli',
  'php56w-devel',
  'php56w-gd',
  'php56w-intl',
  'php56w-mbstring',
  'php56w-mcrypt',
  'php56w-mysql',
  'php56w-pdo',
  'php56w-pecl-imagick',
  'php56w-pecl-memcache',
  'php56w-pecl-xdebug',
  'php56w-process',
  'php56w-xml'
]

package ['libyaml', 'gcc', 'libyaml-devel']

execute 'install yaml' do
    command 'pecl uninstall yaml; pecl install yaml'
end
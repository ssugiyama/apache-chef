#
# Cookbook Name:: php
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

remote_file '/tmp/webtatic.rpm' do
  source 'https://mirror.webtatic.com/yum/el7/webtatic-release.rpm'
  owner 'root'
  group 'root'
end

rpm_package '/tmp/webtatic.rpm' do
  action :install
end

package 'php56w' do
  action :install
end

package 'composer' do
  action :install
end

include_recipe 'php::lib'

template '/etc/php.ini' do
  source 'php.erb'
  owner 'root'
  group 'root'
  mode 0644
end

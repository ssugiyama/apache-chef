#
# Cookbook Name:: httpd
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

package 'httpd' do
  action :install
end

template '/etc/httpd/conf/httpd.conf' do
  source 'httpd.erb'
  owner 'root'
  group 'root'
  mode 0644
end

template '/etc/httpd/conf.d/phpMyAdmin.conf' do
    source 'phpmyadmin.erb'
    owner 'root'
    group 'root'
    mode 0644
end

service 'httpd' do
  supports :status => true
  action [ :enable, :start ]
end

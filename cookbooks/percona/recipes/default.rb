#
# Cookbook Name:: percona
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

remote_file '/tmp/percona.rpm' do
  source 'http://www.percona.com/downloads/percona-release/redhat/0.1-3/percona-release-0.1-3.noarch.rpm'
  owner 'root'
  group 'root'
end

rpm_package '/tmp/percona.rpm' do
  action :install
end

package 'Percona-Server-server-55' do
  action :install
end

service 'mysql' do
  supports :status => true
  action [ :enable, :start ]
end

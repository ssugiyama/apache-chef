#
# Cookbook Name:: dev
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

package 'epel-release' do
  action :install
end

include_recipe 'dev::cache'
include_recipe 'dev::httpd'
include_recipe 'dev::php'
include_recipe 'dev::percona'
include_recipe 'dev::phpmyadmin'

service 'httpd' do
  action :restart
end

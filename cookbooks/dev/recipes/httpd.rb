node.default["httpd"] = {
  'base_path' => '/var/www/html/yg/web'
}

include_recipe 'httpd'

directory '/var/www/html' do
  action :delete
  recursive true
  not_if { File.symlink?('var/www/html') }
  notifies :restart, resources( :service => 'httpd' ), :delayed
end

link '/var/www/html' do
  to '/vagrant/dev'
end

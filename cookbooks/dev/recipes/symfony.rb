execute 'install symfony' do
    command 'curl -LsS https://symfony.com/installer -o /usr/local/bin/symfony; chmod a+x /usr/local/bin/symfony'
end

execute 'install symfony base' do
    command 'if [ ! -d /var/www/yg ]; then cd /var/www; symfony new yg; fi'
end
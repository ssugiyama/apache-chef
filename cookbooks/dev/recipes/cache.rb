package ['redis', 'memcached']

service 'redis' do
  supports :status => true
  action [ :enable, :start ]
end

service 'memcached' do
  supports :status => true
  action [ :enable, :start ]
end
